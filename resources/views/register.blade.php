<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Signup</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action='/welcome' method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <Label>First name:</Label> <br><br>
        <input type="text" name="nama_depan"> <br><br>
        <label>Last name:</label><br><br>
        <input type="text" name="nama_belakang"> <br><br>
        <label>Gender:</label> <br><br>
        <input type="radio" nama="gender">Male <br>
        <input type="radio" nama="gender">Female <br>
        <input type="radio" nama="gender">Other <br><br>
        <label>Nationality:</label> <br><br>
        <select name="Nationality">
            <option value="1">Indonesian</option>
            <option value="2">English</option>
            <option value="2">Malaysia</option>
        </select> <br><br>
        <label>Language Spoken:</label> <br><br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br><br>
        <label>Bio:</label> <br><br>
        <textarea name="bio" cols="30" rows="10"></textarea> <br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</body>
</html>