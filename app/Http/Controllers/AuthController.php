<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('register');
    }

    public function welcome() {
        return view('welcome');
    }

    public function welcome_post(Request $request) {
        $nama_depan = $request['nama_depan'];
        $nama_belakang = $request['nama_belakang'];
        $nama_lengkap = $nama_depan . " " . $nama_belakang;
        return view('welcome', ['nama_lengkap' => $nama_lengkap]);

    }
}
